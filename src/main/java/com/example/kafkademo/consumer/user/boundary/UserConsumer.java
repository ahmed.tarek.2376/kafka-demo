package com.example.kafkademo.consumer.user.boundary;

import com.example.kafkademo.consumer.user.control.UserConsumerCtrl;
import com.example.kafkademo.repository.user.boundary.UserRepo;
import com.example.kafkademo.repository.user.entity.UserDocument;
import com.example.kafkademo.resource.user.entity.UserModel;
import com.example.kafkademo.shared.AppConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserConsumer {

    private final UserConsumerCtrl userConsumerCtrl;
    private final UserRepo userRepo;

    @Autowired
    public UserConsumer(UserConsumerCtrl userConsumerCtrl, UserRepo userRepo) {
        this.userConsumerCtrl = userConsumerCtrl;
        this.userRepo = userRepo;
    }

    @KafkaListener(topics = AppConstants.KAFKA_TOPIC_NAME, groupId = AppConstants.KAFKA_GROUP_ID)
    public void consumeUserFromTopic(UserModel user){
        log.info("Consumed: " + user);

        UserDocument userDocument = userConsumerCtrl.getUserDocument(user);
        userRepo.save(userDocument);
    }
}
