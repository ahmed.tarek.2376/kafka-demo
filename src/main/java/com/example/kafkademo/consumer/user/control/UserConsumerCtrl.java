package com.example.kafkademo.consumer.user.control;

import com.example.kafkademo.repository.user.entity.UserDocument;
import com.example.kafkademo.resource.user.entity.UserModel;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
public class UserConsumerCtrl {

    public UserDocument getUserDocument(UserModel userModel) {
        UserDocument userDocument = new UserDocument();
        userDocument.setId(new ObjectId(userModel.getId()));
        userDocument.setName(userModel.getName());
        userDocument.setRole(userModel.getRole());
        return userDocument;
    }
}

