package com.example.kafkademo.resource.trip.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TripsResponse {
    private List<TripModel> trips;
}
