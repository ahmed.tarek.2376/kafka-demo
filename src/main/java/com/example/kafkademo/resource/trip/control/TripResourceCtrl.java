package com.example.kafkademo.resource.trip.control;

import com.example.kafkademo.resource.trip.entity.TripModel;
import com.example.kafkademo.resource.trip.entity.TripsResponse;
import com.example.kafkademo.shared.AppConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class TripResourceCtrl {

    private RestTemplate restTemplate;

    @Autowired
    public TripResourceCtrl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Retryable(value = RuntimeException.class, maxAttempts = 3, backoff = @Backoff(2000))
    public List<TripModel> getTrips() {
        log.info("$$ Calling remote request");
        TripModel[] trips = restTemplate.getForObject(AppConstants.REMOTE_URL, TripModel[].class);
        assert trips != null;
        return Arrays.asList(trips);
    }

    //used to call a remote api that always timeouts
    @Retryable(value = ResourceAccessException.class, maxAttempts = 3, backoff = @Backoff(2000))
    public List<TripModel> timeoutRequest() {
        log.info("$$ Calling remote timeout request");
        TripModel[] trips = restTemplate.getForObject(AppConstants.REMOTE_TIMEOUT_URL, TripModel[].class);
        assert trips != null;
        return Arrays.asList(trips);
    }


    //used to call a remote api that always fails
    @Retryable(value = HttpStatusCodeException.class, maxAttempts = 3, backoff = @Backoff(2000))
    public List<TripModel> failRequest() {
        log.info("$$ Calling remote failing request");
        TripModel[] trips = restTemplate.getForObject(AppConstants.REMOTE_FAILING_URL, TripModel[].class);
        assert trips != null;
        return Arrays.asList(trips);
    }

    @Recover
    public List<TripModel> fallback(){
        return List.of();
    }
}
