package com.example.kafkademo.resource.trip.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TripModel {
    private String fromDestination;
    private String toDestination;
}
