package com.example.kafkademo.resource.user.boundary;

import com.example.kafkademo.resource.user.control.UserResourceCtrl;
import com.example.kafkademo.resource.user.entity.UserModel;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user") //TODO Ask why this is wrong
public class UserResource {

    private UserResourceCtrl userResourceCtrl; //TODO Ask why this approach is not right (All logic there)

    @Autowired
    public UserResource(UserResourceCtrl userResourceCtrl){
        this.userResourceCtrl = userResourceCtrl;
    }

    @PostMapping
    ResponseEntity<?> addUser(@RequestBody UserModel bodyUser){
        userResourceCtrl.produceUserEvent(bodyUser);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    ResponseEntity<List<UserModel>> getAllUsers(){
        List<UserModel> all = userResourceCtrl.getAllUsers();
        return ResponseEntity.ok(all);
    }

}
