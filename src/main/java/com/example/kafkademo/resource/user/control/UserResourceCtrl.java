package com.example.kafkademo.resource.user.control;

import com.example.kafkademo.producer.KafkaProducer;
import com.example.kafkademo.repository.user.boundary.UserRepo;
import com.example.kafkademo.repository.user.entity.UserDocument;
import com.example.kafkademo.resource.user.entity.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserResourceCtrl {

    private KafkaProducer eventProducer;
    private UserRepo userRepo;

    @Autowired
    public UserResourceCtrl(KafkaProducer eventProducer, UserRepo userRepo) {
        this.eventProducer= eventProducer;
        this.userRepo = userRepo;
    }


    public List<UserModel> mapUsersListFromEntities(List<UserDocument> userDocumentList) {
        List<UserModel> userModels = new ArrayList<>();
        userDocumentList.stream().map(this::mapUserModelFromEntity).
                forEach(userModels::add);
        return userModels;
    }

    public UserModel mapUserModelFromEntity(UserDocument userDocument) {
        UserModel userModel = new UserModel();
        userModel.setId(userDocument.getId().toString());
        userModel.setName(userDocument.getName());
        userModel.setRole(userDocument.getRole());
        return userModel;
    }

    public void produceUserEvent(UserModel userModel) {
        eventProducer.sendUserEvent(userModel);
    }

    public List<UserModel> getAllUsers() {
        return mapUsersListFromEntities(userRepo.findAll());
    }
}
