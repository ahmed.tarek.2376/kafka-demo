package com.example.kafkademo.producer;

import com.example.kafkademo.resource.user.entity.UserModel;
import com.example.kafkademo.shared.AppConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaProducer {

    private final KafkaTemplate<String, UserModel> kafkaTemplate;

    @Autowired
    public KafkaProducer(KafkaTemplate<String, UserModel> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }


    public void sendUserEvent(UserModel user) {
        String topic = AppConstants.KAFKA_TOPIC_NAME;
        log.info("Sending payload= " + user + " to topic= " + topic);
        kafkaTemplate.send(topic, user);
    }

}
