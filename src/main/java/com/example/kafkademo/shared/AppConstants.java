package com.example.kafkademo.shared;

public class AppConstants {

    public static final String KAFKA_TOPIC_NAME = "user-events";
    public static final String KAFKA_GROUP_ID = "createUser";

    public static final String REMOTE_FAILING_URL = "http://localhost:8081/api/trip/all/fail";
    public static final String REMOTE_TIMEOUT_URL = "http://localhost:8081/api/trip/all/timeout";
    public static final String REMOTE_URL = "http://localhost:8081/api/trip/all/";
}
