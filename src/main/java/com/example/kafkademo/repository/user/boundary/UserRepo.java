package com.example.kafkademo.repository.user.boundary;

import com.example.kafkademo.repository.user.entity.UserDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends MongoRepository<UserDocument, String> {
}
